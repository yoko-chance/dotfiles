require('basic')

do -- plugins management
  require('plugins.dpp')

  local dppCacheDir = vim.fn.expand('~/.cache/dpp')
  if vim.fn.isdirectory(dppCacheDir) == 0 then
    vim.fn.mkdir(dppCacheDir, 'p')
  end

  dpp_init(dppCacheDir, vim.fn.expand('~/.config/nvim/dpp.config.ts'))
  -- dpp_plugins_setup() -- TODO: implement this
end

vim.cmd('filetype indent plugin on')
vim.cmd('syntax on')

vim.env.NVIM_LISTEN_ADDRESS = vim.v.servername
