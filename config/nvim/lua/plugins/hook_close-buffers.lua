-- lua_post_source {{{
require('close_buffers').setup {}

vim.keymap.set(
  'n',
  '<leader>bdt',
  [[<CMD>lua require('close_buffers').delete({type = 'this'})<CR>]],
  {silent = true, desc ='Delete current buffer'}
)
vim.keymap.set(
  'n',
  '<leader>bdo',
  [[<CMD>lua require('close_buffers').delete({type = 'other'})<CR>]],
  {silent = true, desc ='Delete other buffers'}
)
-- }}}
