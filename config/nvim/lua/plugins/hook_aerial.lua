-- lua_post_source {{{
require("aerial").setup({
  layout = {
    default_direction = "float",
    placement = "window",
    min_width = 0.6,
  },
  float = {
    border = "single",
    relative = "win",
    min_height = 0.5,
  },
  on_attach = function(bufnr)
    vim.keymap.set("n", "{", "<cmd>AerialPrev<CR>", { buffer = bufnr })
    vim.keymap.set("n", "}", "<cmd>AerialNext<CR>", { buffer = bufnr })
  end,
})

vim.keymap.set("n", "<leader>a", "<cmd>AerialToggle<CR>")
-- }}}
