-- lua_add {{{
vim.g.asyngrep_cfg_path = "~/.config/nvim/dein/conf.d/.asyngrep.toml"
vim.api.nvim_set_keymap('n', '<Leader>ss', ':Agp<CR>', {noremap = true})
vim.api.nvim_set_keymap('n', '<Leader>st', ':Agp --tool=', {noremap = true})
-- }}}
