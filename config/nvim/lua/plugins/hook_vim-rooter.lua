-- lua_add {{{
vim.g.rooter_cd_cmd = 'lcd'
vim.g.rooter_silent_chdir = 1
vim.g.rooter_patterns = {
  '.git', '_darcs', '.hg', '.bzr', '.svn',
  'go.mod', 'composer.json', 'package.json',
  'Makefile',
}
-- vim.g.rooter_resolve_links = 1
-- }}}
