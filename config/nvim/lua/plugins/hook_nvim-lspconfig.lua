-- lua_post_source {{{
local lsp = require('lspconfig')
if vim.fn.executable('intelephense') then
  lsp.intelephense.setup {
    root_dir = function(fname)
      return lsp.util.root_pattern('composer.json', 'index.php')(fname) or
             vim.fn.getcwd()
    end,
    on_attach = function(client, bufnr)
      require('lsp_signature').on_attach({}, bufnr)
    end,
    settings = {
      intelephense = {
        stubs = {
          -- stubs reference
          -- - https://github.com/bmewburn/vscode-intelephense/blob/master/package.json
          -- - https://github.com/JetBrains/phpstorm-stubs/
          'Core',
          'PDO',
          'Phar',
          'Reflection',
          'SPL',
          'SimpleXML',
          'apache',
          'bcmath',
          'ctype',
          'curl',
          'date',
          'dom',
          'fileinfo',
          'filter',
          'ftp',
          'gd',
          'hash',
          'iconv',
          'json',
          'laravel',
          'libxml',
          'mbstring',
          'mysqlnd',
          'openssl',
          'pcntl',
          'pcre',
          'pdo_mysql',
          'random',
          'readline',
          'session',
          'shmop',
          'sodium',
          'standard',
          'tokenizer',
          'xml',
          'xmlreader',
          'xmlwriter',
          'zip',
        },
      }
    }
  }
end

if vim.fn.executable('typescript-language-server') then
  lsp.tsserver.setup {
    on_attach = function(client, bufnr)
      require('lsp_signature').on_attach({}, bufnr)
    end,
  }
end

if vim.fn.executable('gopls') then
  lsp.gopls.setup {
    on_attach = function(client, bufnr)
      require('lsp_signature').on_attach({}, bufnr)
    end,
  }

  vim.api.nvim_create_autocmd('BufWritePre', {
    pattern = '*.go',
    callback = function()
      local params = vim.lsp.util.make_range_params()
      params.context = {only = {'source.organizeImports'}}
      -- buf_request_sync defaults to a 1000ms timeout. Depending on your
      -- machine and codebase, you may want longer. Add an additional
      -- argument after params if you find that you have to write the file
      -- twice for changes to be saved.
      -- E.g., vim.lsp.buf_request_sync(0, 'textDocument/codeAction', params, 3000)
      local result = vim.lsp.buf_request_sync(0, 'textDocument/codeAction', params)
      for cid, res in pairs(result or {}) do
        for _, r in pairs(res.result or {}) do
          if r.edit then
            local enc = (vim.lsp.get_client_by_id(cid) or {}).offset_encoding or 'utf-16'
            vim.lsp.util.apply_workspace_edit(r.edit, enc)
          end
        end
      end
      vim.lsp.buf.format({async = false})
    end
  })
end

if vim.fn.executable('marksman') then
  lsp.marksman.setup {}
end
-- }}}
