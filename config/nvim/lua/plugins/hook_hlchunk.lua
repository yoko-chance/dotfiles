-- lua_post_source {{{
require("hlchunk").setup({
  indent = { enable = true, },
  chunk  = { enable = false, },
  blank  = { enable = false, },
})
-- }}}
