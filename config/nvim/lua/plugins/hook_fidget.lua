-- lua_post_source {{{
require('fidget').setup {
  notification = {
    filter              = vim.log.levels.INFO,
    override_vim_notify = true,
  },
}
-- }}}
