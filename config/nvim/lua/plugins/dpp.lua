function dpp_init(cacheDir, tsConfigPath)
  local installedReposDir = 'repos'

  -- NOTE: write it also in hasty.toml
  local dppRepo             = 'github.com/Shougo/dpp.vim'
  local denopsRepo          = 'github.com/vim-denops/denops.vim'
  local dppExtInstallerRepo = 'github.com/Shougo/dpp-ext-installer'
  local dppExtTomlRepo      = 'github.com/Shougo/dpp-ext-toml'
  local dppExtLazyRepo      = 'github.com/Shougo/dpp-ext-lazy'
  local dppExtGitRepo       = 'github.com/Shougo/dpp-protocol-git'

  local dppSrc          = cacheDir .. '/' .. installedReposDir .. '/' .. dppRepo
  local denopsSrc       = cacheDir .. '/' .. installedReposDir .. '/' .. denopsRepo
  local dppInstallerSrc = cacheDir .. '/' .. installedReposDir .. '/' .. dppExtInstallerRepo
  local dppTomlSrc      = cacheDir .. '/' .. installedReposDir .. '/' .. dppExtTomlRepo
  local dppLazySrc      = cacheDir .. '/' .. installedReposDir .. '/' .. dppExtLazyRepo
  local dppGitSrc       = cacheDir .. '/' .. installedReposDir .. '/' .. dppExtGitRepo

  dpp_install_if_no_installed(dppSrc,          'https://' .. dppRepo)
  dpp_install_if_no_installed(denopsSrc,       'https://' .. denopsRepo)
  dpp_install_if_no_installed(dppInstallerSrc, 'https://' .. dppExtInstallerRepo)
  dpp_install_if_no_installed(dppTomlSrc,      'https://' .. dppExtTomlRepo)
  dpp_install_if_no_installed(dppLazySrc,      'https://' .. dppExtLazyRepo)
  dpp_install_if_no_installed(dppGitSrc,       'https://' .. dppExtGitRepo)

  vim.opt.runtimepath:prepend(dppSrc)
  local dpp = require('dpp')

  vim.opt.runtimepath:prepend(dppInstallerSrc)
  vim.opt.runtimepath:prepend(dppTomlSrc)
  vim.opt.runtimepath:prepend(dppLazySrc)
  vim.opt.runtimepath:prepend(dppGitSrc)


  if dpp.load_state(cacheDir) then
    vim.opt.runtimepath:prepend(denopsSrc)
    vim.api.nvim_create_autocmd("User", {
      pattern = "DenopsReady",
      callback = function()
        dpp.make_state(cacheDir, tsConfigPath)

        if dpp.load_state(cacheDir) then
          vim.notify("dpp load_state() is failed")
        end
      end,
    })
  end

  vim.api.nvim_create_user_command('MakeDppState', function()
    dpp.make_state(cacheDir, tsConfigPath)

    if dpp.load_state(cacheDir) then
      vim.notify("dpp load_state() is failed")
    end
  end, {})

  vim.api.nvim_create_autocmd("User", {
    pattern = "Dpp:makeStatePost",
    callback = function()
      vim.notify("dpp make_state() is done")
    end,
  })

  -- Update state file
  -- call dpp#check_files()

  -- Install plugins
  -- call dpp#async_ext_action('installer', 'install')

  -- Update plugins
  -- call dpp#async_ext_action('installer', 'update')

  -- Update dpp.vim
  -- call dpp#async_ext_action('installer', 'update', #{ names: ['dpp.vim'] })

  -- Check not updated plugins
  -- call dpp#async_ext_action('installer', 'checkNotUpdated')
end

function dpp_plugins_setup()
  local dpp = require('dpp')

  vim.api.nvim_create_autocmd("User", {
    pattern = "DenopsReady",
    callback = function()
      local notInstalls = dpp.sync_ext_action('installer', 'getNotInstalled')

      if #notInstalls == 0 then
        return
      end

      for _, notInstall in ipairs(notInstalls) do
        vim.notify('Install ' .. notInstall.name)
        -- dpp.async_ext_action('installer', 'install', { names = {notInstall.name} })
      end
    end,
  })
end

function dpp_install_if_no_installed(installedDir, repoUrl)
  if vim.fn.isdirectory(installedDir) ~= 1 then
    vim.cmd('!git clone ' .. repoUrl .. ' ' .. installedDir)
  end
end
