-- lua_post_source {{{
vim.fn['ddc#custom#patch_global']({
  ui = 'pum',
  sources = { 'lsp', 'around', 'dictionary', },
  sourceOptions = {
    _ = {
      matchers = { 'matcher_head' },
      sorters  = { 'sorter_rank' },
    },
    around     = { mark = 'a', },
    dictionary = { mark = 'D' },
    lsp        = {
        mark = 'lsp',
        forceCompletionPattern = '\\.\\w*|:\\w*|->\\w*',
    },
  },
  sourceParams = {
    around = { maxSize = 500 },
    dictionary  = {
      dictPaths = {
        '/usr/share/dict/words',
        '/usr/share/dict/british-english',
      },
      smartCase  = true,
      isVolatile = true,
    },
    lsp = {
      snippetEngine = vim.fn['denops#callback#register']({
        function(body)
          vim.fn['vsnip#anonymous'](body)
        end
      }),
      enableResolveItem        = true,
      enableAdditionalTextEdit = true,
    },
  },
})

vim.keymap.set('i', '<C-n>',      '<Cmd>call pum#map#insert_relative(+1)<CR>')
vim.keymap.set('i', '<C-p>',      '<Cmd>call pum#map#insert_relative(-1)<CR>')
vim.keymap.set('i', '<C-y>',      '<Cmd>call pum#map#confirm()<CR>')
vim.keymap.set('i', '<C-e>',      '<Cmd>call pum#map#cancel()<CR>')
vim.keymap.set('i', '<PageDown>', '<Cmd>call pum#map#insert_relative_page(+1)<CR>')
vim.keymap.set('i', '<PageUp>',   '<Cmd>call pum#map#insert_relative_page(-1)<CR>')

vim.fn['ddc#enable']()
-- }}}
