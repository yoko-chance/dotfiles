-- lua_post_source {{{
vim.g.barbar_auto_setup = false
require('barbar').setup {
  icons = {
    buffer_index = true,
    filetype = { custom_colors = true }
  }
}

function hiBuffer(bufType, bg, fg)
  vim.cmd.hi('Buffer' .. bufType,             'guibg=' .. bg, 'guifg=' .. fg)
  vim.cmd.hi('Buffer' .. bufType .. 'Mod',    'guibg=' .. bg, 'guifg=' .. fg)
  vim.cmd.hi('Buffer' .. bufType .. 'Index',  'guibg=' .. bg, 'guifg=' .. fg)
  vim.cmd.hi('Buffer' .. bufType .. 'Icon',   'guibg=' .. bg, 'guifg=' .. fg)
  vim.cmd.hi('Buffer' .. bufType .. 'Sign',   'guibg=' .. bg, 'guifg=' .. fg)
  vim.cmd.hi('Buffer' .. bufType .. 'ModBtn', 'guibg=' .. bg, 'guifg=#cb4b16')
end

hiBuffer('Current',  '#eee8d5', '#657b83')
hiBuffer('Visible',  '#657b83', '#eee8d5')
hiBuffer('Inactive', '#073642', '#93a1a1')
-- }}}

-- lua_add {{{
local opts = {noremap = true, silent = true}
vim.keymap.set('n', '<C-h>', '<Cmd>BufferPrevious<CR>', opts)
vim.keymap.set('n', '<C-l>', '<Cmd>BufferNext<CR>',     opts)
vim.keymap.set('n', '<leader>bg', ':BufferGoto ',       opts)
-- }}}
