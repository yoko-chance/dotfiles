-- lua_post_source {{{
require('nvim-treesitter.configs').setup {
  auto_install = false,
  highlight = {
    disable = {"markdown"},
    enable = true,
  },
  indent = {
    enable = true,
  },
  ensure_installed = {
    "bash",
    "csv", "json", "yaml",
    "dockerfile",
    "go",
    "html", "css", "javascript", "tsx",
    "ini",
    "lua",
    "php",
    "printf",
    "ssh_config",
    "vim",
  },
}
-- }}}
