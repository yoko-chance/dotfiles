-- lua_post_source {{{
require("no-neck-pain").setup({ width = 150, })

vim.api.nvim_set_keymap('n', '<Leader>np', ':NoNeckPain<CR>', {noremap = true})
-- }}}
