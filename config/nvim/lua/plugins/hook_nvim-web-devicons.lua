-- lua_post_source {{{
require('nvim-web-devicons').setup {
  color_icons = true;
  default     = true;
}
-- }}}
