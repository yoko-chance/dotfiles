vim.opt.clipboard:append({'unnamedplus'})
vim.opt.backspace:append({'indent', 'eol', 'start'})
vim.opt.matchpairs:append({'<:>'})

vim.g.python3_host_prog = vim.fn.expand(vim.env.NVIM_PYTHON3_PATH)

vim.o.termguicolors =true

vim.o.backup     = false
vim.o.showmatch  = true
vim.o.matchtime  = 1
vim.o.display    = 'lastline'
vim.o.pumheight  = 10
vim.o.mouse      = ''
vim.o.scrollback = 500

vim.o.modeline  = true
vim.o.modelines = 5

vim.o.list      = true
vim.o.listchars = 'trail:#,tab::.'

vim.o.laststatus = 2
vim.o.signcolumn     = 'yes'
vim.o.number         = true
vim.o.relativenumber = true

vim.o.cursorline   = true
vim.o.cursorcolumn = true

vim.o.encoding     = 'utf-8'
vim.o.fileencoding = 'utf-8'

vim.o.shiftround  = true
vim.o.smartindent = true

vim.o.ignorecase = true
vim.o.smartcase  = true
vim.o.wrapscan   = true
vim.o.hlsearch   = true

-- `:w]` behaves like `:w`
vim.cmd[[cabbrev <expr> w] (getcmdtype() ==# ":" && getcmdline() ==# "w]") ? "w" : "w]"]]

local swp_dir = vim.env.HOME .. '/.swp'
if vim.fn.isdirectory(swp_dir) == 0 then
  vim.fn.mkdir(swp_dir, 'p')
end
vim.o.swapfile  = true
vim.o.directory = swp_dir .. '//' -- see https://vim-jp.org/vimdoc-en/options.html#'directory'

vim.api.nvim_create_augroup('sethl', {} )
vim.api.nvim_create_autocmd('ColorScheme', {
    group    = 'sethl',
    nested   = true,
    callback = function()
      vim.fn.matchadd('ExtraWhitespace', '[\\u00A0\\u2000-\\u200B\\u3000]')
      local bgcolor='magenta'
      --local bgcolor='#d33682'
      vim.api.nvim_set_hl(0, "ExtraWhitespace", { ctermbg=bgcolor, bg=bgcolor })
    end,
})

-- `:vim` behaves like `:vim ~ | cw`
vim.api.nvim_create_augroup('vimgrep', {} )
vim.api.nvim_create_autocmd('QuickFixCmdPost', {
    group    = 'vimgrep',
    callback = function() vim.cmd[[cwindow]] end,
})

vim.api.nvim_create_augroup('giteditor', {} )
vim.api.nvim_create_autocmd('FileType', {
    group    = 'giteditor',
    pattern  = 'gitcommit,gitrebase,gitconfig',
    callback = function() vim.o.bufhidden = 'delete' end,
})

-- if exist swapfile, that file open read only
vim.api.nvim_create_augroup('readonlyexistswap', {} )
vim.api.nvim_create_autocmd('SwapExists', {
    group    = 'readonlyexistswap',
    callback = function() vim.cmd[[let v:swapchoice = 'o']] end,
})

-- require execute `profile stop` when after execute this command.
vim.api.nvim_create_user_command(
  'Profile',
  function()
    vim.cmd[[
      profile start ~/nvim.profile.txt
      profile func *
      profile file *
    ]]
  end,
  {}
)

vim.o.timeoutlen = 500
vim.g.mapleader = ' '
vim.api.nvim_set_keymap('n', 'Y', 'y$', {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', 'Q', '', {})
vim.api.nvim_set_keymap('n', '<Esc><Esc>', ':noh<CR>',   {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', '<S-Tab>', ':tabprev<CR>',   {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', '<Tab>',   ':tabnext<CR>',   {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', '<Leader>n', ':setlocal relativenumber!<CR>',   {noremap = true, silent = true})
vim.api.nvim_set_keymap('t', '<C-]>', [[<C-\><C-n>]],   {noremap = true, silent = true})

-- https://zenn.dev/vim_jp/articles/2024-10-07-vim-insert-uppercase
vim.keymap.set("i", "<C-l>",
  function()
    local line = vim.fn.getline(".")
    local col = vim.fn.getpos(".")[3]
    local substring = line:sub(1, col - 1)
    local result = vim.fn.matchstr(substring, [[\v<(\k(<)@!)*$]])
    return "<C-w>" .. result:upper()
  end,
  {expr = true}
)

-- lsp
vim.keymap.set('n', '<Leader>ld', vim.lsp.buf.definition, {silent = true})
vim.keymap.set('n', '<Leader>lf', vim.lsp.buf.format,     {silent = true})
vim.keymap.set('n', '<Leader>lc', vim.lsp.buf.rename,     {silent = true})

vim.g.editorconfig = true

if vim.fn.has("wsl") then
  local copy_cmd  = '';
  local paste_cmd = '';

  if vim.fn.has("win32yank") then
    copy_cmd  = 'win32yank -i';
    paste_cmd = 'win32yank -o';

  elseif vim.fn.has("win32yank.exe") then
    copy_cmd  = 'win32yank.exe -i';
    paste_cmd = 'win32yank.exe -o';
  end

  vim.g.clipboard = {
    name = 'myClipboard',
    copy = {
      ['+'] = copy_cmd,
      ['*'] = copy_cmd,
    },
    paste = {
      ['+'] = paste_cmd,
      ['*'] = paste_cmd,
    },
    cache_enabled = 1,
  }
end

if vim.fn.has('nvr') then
  vim.env.GIT_EDITOR = 'nvr -cc split --remote-wait'
end
