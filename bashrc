
alias      vi='nvim'
alias     vim='nvim'
alias vimdiff='nvim -d'
alias    view='nvim -R'
alias      ls='ls --color'
alias      ll='ls -l'
alias     ssh='TERM=xterm ssh'

[ -x "$(which gls)" ] && alias ls='gls --color=auto'

if [ -x "$(which ghq)" ]; then
  alias gg='ghq get'

  if [ -x "$(which fzf)" ]; then
    alias gl='ghq list | fzf'

    g() {
      repo="$(gl)"
      code="$?"

      [ -z "${repo}" ] && return "${code}"

      cd "$(ghq root)/${repo}"
    }
  fi
fi

# brew
[ -x "/home/linuxbrew/.linuxbrew/bin/brew" ] && eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

# completion
b_comp="/usr/share/bash-completion/bash_completion"
[[ -f "${b_comp}" ]] && source "${b_comp}"
if type brew &>/dev/null; then
  brew_prefix="$(brew --prefix)"

  bash_comp_sh=${brew_prefix}/etc/profile.d/bash_completion.sh
  [[ -r "${bash_comp_sh}" ]] && source "${bash_comp_sh}"

  bash_comp_dir=${brew_prefix}/etc/bash_completion.d
  if [[ -d "${bash_comp_dir}" ]]; then
    for comp in "${bash_comp_dir}/"*; do
      [[ -r "$comp" ]] && source "$comp"
    done
  fi
fi

# asdf
for asdf_sh in "$(brew --prefix asdf)/asdf.sh" "${HOME}/.asdf/asdf.sh"; do
  [[ -f ${asdf_sh} ]] && source ${asdf_sh} && break
done

type aws_completer &>/dev/null && complete -C "$(which aws_completer)" aws
type kubectl &>/dev/null && source <(kubectl completion bash)
type helm    &>/dev/null && source <(helm completion bash)
type kind    &>/dev/null && source <(kind completion bash)

fzf_bash=${HOME}/.fzf.bash
[ -f ${fzf_bash} ] && source ${fzf_bash}

[ -x "`which gdircolors`" ] && eval `gdircolors ${HOME}/.dir_colors`

eval "$(direnv hook bash)"
# if you want to ignore execution of direnv, run the following command
# cd /path/to/directory && `echo 'exit' > .envrc && direnv allow`

[ -f "${HOME}/.fzf.bash" ] && source "${HOME}/.fzf.bash"
[ -f "${HOME}/.bashrc_local" ] && source "${HOME}/.bashrc_local"

eval "$(starship init bash)"

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

if [ -f ~/.ssh-agent ]; then
    . ~/.ssh-agent
fi
if [ -z "$SSH_AGENT_PID" ] || ! kill -0 $SSH_AGENT_PID; then
    ssh-agent -t 43200 > ~/.ssh-agent
    . ~/.ssh-agent
fi

# nvim-remote
[ -z ${NVIM_LISTEN_ADDRESS} ] && { nvim; exit; }
