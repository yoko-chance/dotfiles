Dotfiles
========
1. install [rcm](https://github.com/thoughtbot/rcm#installation) and git
2. execute `git clone ssh://git@gitlab.com/yoko-chance/dotfiles.git "${HOME}/.dotfiles" && cd "$_" && rcup`

and install
- [brew](https://brew.sh/)
- [ghq](https://github.com/x-motemen/ghq#installation)
- [asdf](https://github.com/asdf-vm/asdf#documentation)
- [neovim (latest stable version)](https://github.com/neovim/neovim/blob/master/INSTALL.md#install-from-package)
- [neovim-remote](https://github.com/mhinz/neovim-remote#installation)
- [fzf](https://github.com/junegunn/fzf#installation)
- [direnv](https://github.com/direnv/direnv#getting-started)
- [deno](https://docs.deno.com/runtime/manual/getting_started/installation)
- [starship](https://starship.rs/guide/#%F0%9F%9A%80-installation)
- [word list for look command](http://wordlist.aspell.net)
- [lazygit](https://github.com/jesseduffield/lazygit#installation)
- [ripgrep](https://github.com/BurntSushi/ripgrep#installation)
- [pt](https://github.com/monochromegane/the_platinum_searcher#installation)
- [jvgrep](https://github.com/mattn/jvgrep)
- [teip](https://github.com/greymd/teip?tab=readme-ov-file#installation)
- [gopls](https://github.com/golang/tools/tree/master/gopls#installation)
- [marksman](https://github.com/artempyanykh/marksman/tree/main?tab=readme-ov-file#how-to-install)
- [nerd fonts](https://www.nerdfonts.com/)
- [Tree-sitter CLI](https://formulae.brew.sh/formula/tree-sitter)
- [cz-git](https://cz-git.qbb.sh/guide/#as-global-use)

and more...  
**TODO list up**

Neovim
------
when first launched up neovim, execute `call dpp#async_ext_action('installer', 'install')`
