hs.loadSpoon("ShiftIt")
spoon.ShiftIt:bindHotkeys({
  left = {{ 'cmd' }, 'left' },
  right = {{ 'cmd' }, 'right' },
  up = {{ 'cmd' }, 'up' },
  down = {{ 'cmd' }, 'down' },
  upright = {{ 'alt', 'cmd' }, 'right' },
  botright = {{ 'alt', 'cmd' }, 'down' },
  botleft = {{ 'alt', 'cmd' }, 'left' },
  upleft = {{ 'alt', 'cmd' }, 'up' },
  maximum = {{ 'alt', 'cmd' }, 'm' },
  toggleFullScreen = {{ 'alt', 'cmd' }, 'f' },
  toggleZoom = {{ 'alt', 'cmd' }, 'z' },
  center = {{ 'alt', 'cmd' }, 'c' },
  nextScreen = {{ 'alt', 'cmd' }, 'n' },
  previousScreen = {{ 'alt', 'cmd' }, 'p' },
  resizeOut = {{ 'alt', 'cmd' }, ';' },
  resizeIn = {{ 'alt', 'cmd' }, '-' }
})