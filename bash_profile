# vim: set ft=sh :

export TERM=xterm-256color
export EDITOR="nvr -cc split -c 'set bh=wipe' --remote-wait"

[ -x "$(which asdf)" ] && [ -d "$(asdf where golang)" ] && PATH="${PATH}:$(asdf where golang)/packages/bin"
[ -d "${KREW_ROOT:-$HOME/.krew}" ] && PATH="${PATH}:${KREW_ROOT:-$HOME/.krew}/bin"
[ -d "/Applications/SuperCollider.app/Contents/MacOS" ] && PATH="${PATH}:/Applications/SuperCollider.app/Contents/MacOS" # macos
PATH="/usr/local/go/bin:${PATH}"
PATH="${HOME}/go/bin:${PATH}"
PATH="${HOME}/bin:${PATH}"
PATH="${HOME}/.local/bin:${PATH}"
PATH="/usr/local/opt/libpq/bin:${PATH}"
PATH="/usr/local/opt/openssl@3/bin:${PATH}"
PATH="${PATH}:/opt/local/bin:/opt/local/sbin"
PATH="${PATH}:${HOME}/google-cloud-sdk/bin"

export PATH

[ -f "${HOME}/.bash_profile_local" ] && source "${HOME}/.bash_profile_local"

[ -f "${HOME}/.bashrc" ] && source "${HOME}/.bashrc"

# The next line updates PATH for the Google Cloud SDK.
[ -f "${HOME}/google-cloud-sdk/path.bash.inc" ] && . "${HOME}/google-cloud-sdk/path.bash.inc"
# The next line enables shell command completion for gcloud.
[ -f "${HOME}/google-cloud-sdk/completion.bash.inc" ] && . "${HOME}/google-cloud-sdk/completion.bash.inc"

[ -f "${HOME}/.iterm2_shell_integration.bash" ] && . "${HOME}/.iterm2_shell_integration.bash"
